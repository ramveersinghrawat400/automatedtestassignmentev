Feature: Bus depot charging task

  Scenario Outline: As a user, I want to decide how much an EV should be charged or discharged, given the time of day
    When I charge ev at '<time>' which have EV '<evType>' and current charge <chargePercentage> %
    Then the ev should be '<Decision>'

    Examples:
    |evType|chargePercentage|time|Decision|
    |School Bus| 30          | 06:00|Charge battery to 90%|
    |School Bus| 30          | 09:00|Discharge battery to 50%|
    |School Bus| 55          | 20:00|Discharge battery to 30%|
    |Commuter bus| 30        | 04:00|Charge battery to 95%|
    |Commuter bus| 50        | 23:30|Discharge battery to 30%|
    |Bus|49|15:00|Charge battery to 80%|
    |Bus|55|15:00|Charge battery to 70%|
    |Bus|65|15:00|Discharge battery to 50%|

