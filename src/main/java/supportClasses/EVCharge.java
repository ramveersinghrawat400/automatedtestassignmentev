package supportClasses;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EVCharge {
    public String  getChargeDifferential(String evType, int chargePerc, Date time ) throws ParseException {
        String decision=null;
        SimpleDateFormat parser=new SimpleDateFormat("HH:mm");
        if(evType.equalsIgnoreCase("School Bus")){
            if(time.before(parser.parse("08:00"))){
                decision="Charge battery to 90%";
            }
            else if(time.after(parser.parse("08:00")) && time.before(parser.parse("11:00"))){
                decision="Discharge battery to 50%";

            }
            else if(time.after(parser.parse("06:00")) && time.before(parser.parse("24:00"))&& chargePerc>=50 ){
                decision="Discharge battery to 30%";
            }

        }
        else  if(evType.equalsIgnoreCase("Commuter Bus")){
            if(time.after(parser.parse("03:00")) && time.before(parser.parse("07:00"))){
                decision="Charge battery to 95%";
            }
            else if (time.after(parser.parse("11:15"))){
                decision="Discharge battery to 30%";
            }
        }
        else{
            if(chargePerc<50){
                decision="Charge battery to 80%";
            }
            else if(chargePerc>50 && chargePerc<=60){
                decision="Charge battery to 70%";
            }
            else if(chargePerc>=60){
                decision="Discharge battery to 50%";
            }
        }

        return decision;
    }
}
