package stepDefinition;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import supportClasses.EVCharge;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class StepDefinition {
    EVCharge evCharge= new EVCharge();
    String decision=null;

    @When("I charge ev at {string} which have EV {string} and current charge {int} %")
    public void i_charge_ev_at_which_have_ev_and_current_charge(String time,String evtype, Integer chargePerc) throws ParseException {
        SimpleDateFormat timeformat =new SimpleDateFormat("HH:mm");
        Date retime= timeformat.parse(time);
        decision = evCharge.getChargeDifferential(evtype,chargePerc,retime);
    }

    @Then("the ev should be {string}")
    public void the_ev_should_be(String dec) {
        assertEquals(dec,decision);
    }
}
