Overview

This is the automation project for Ev Charging Task
Implemeneted getChangeDifferential() method at src/main/java/supportClasses/EvCharge.class

The cucumber BDD testing framework specifies acceptance tests as. Using keywords such as Given, When, Then and And, acceptance criteria tests known as feature files can then be broken down into testable steps. Cucumber BDD framework is using JAVA as programming language to support the Framework.


Feature File - The feature file specifies the steps in BDD language style

Step Definition Feature File - Java class whereby the steps from the feature file are broken down to be coded into automation tests

How to Run: Go to the src/test/java/com/test/TestRunner.class class and right click and select run test runner

Report: This framework includes cucumber .json, and cucumber.html test report